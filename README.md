# ASP.NET Core Identity with Fido2 WebAuthn MFA

https://damienbod.com/2019/08/06/asp-net-core-identity-with-fido2-webauthn-mfa/

## History

2019-08-13 Updated to .NET Core preview 8

## Links

https://github.com/abergs/fido2-net-lib

https://www.youtube.com/watch?v=qgZ3JO2khFg

https://www.yubico.com/products/yubikey-hardware/

https://www.youtube.com/watch?v=2KfZJRsacNM

https://www.troyhunt.com/beyond-passwords-2fa-u2f-and-google-advanced-protection/

https://fidoalliance.org/fido2/

https://www.w3.org/TR/webauthn/

https://www.scottbrady91.com/FIDO/A-FIDO2-Primer-and-Proof-of-Concept-using-ASPNET-Core

https://github.com/herrjemand/awesome-webauthn

https://developers.yubico.com/FIDO2/Libraries/Using_a_library.html

https://medium.com/@herrjemand

https://github.com/w3c/webauthn